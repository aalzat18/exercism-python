def to_rna(dna_strand):
    for x in dna_strand:
        if x is 'G':
            dna_strand = dna_strand.replace(x, 'c')
        elif x is 'C':
            dna_strand = dna_strand.replace(x, 'g')
        elif x is 'T':
            dna_strand = dna_strand.replace(x, 'a')
        elif x is 'A':
            dna_strand = dna_strand.replace(x, 'u')
        else:
            raise ValueError ("ValueError")
    return dna_strand.upper()
    #pass